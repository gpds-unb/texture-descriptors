message("External project: OpenCV")

include(ExternalProject)

if( NOT OpenCV_TAG )
  set( OpenCV_TAG "4.1.1" )
endif()


#  GIT_PROGRESS 1

ExternalProject_Add(OpenCV
  GIT_REPOSITORY https://github.com/opencv/opencv.git
  GIT_TAG "${OpenCV_TAG}"
  SOURCE_DIR opencv
  BINARY_DIR opencv-build
  CMAKE_GENERATOR ${gen}
  CMAKE_ARGS
    -DBUILD_DOCS:BOOL=OFF
    -DBUILD_EXAMPLES:BOOL=OFF
    -DBUILD_NEW_PYTHON_SUPPORT:BOOL=OFF
    -DBUILD_WITH_DEBUG_INFO=OFF
    -DBUILD_PACKAGE:BOOL=OFF
    -DBUILD_opencv_core=ON
    -DBUILD_opencv_imgproc=ON
    -DBUILD_opencv_highgui=ON
    -DBUILD_opencv_video=OFF
    -DBUILD_opencv_apps=OFF
    -DBUILD_opencv_flann=OFF
    -DBUILD_opencv_gpu=OFF
    -DBUILD_opencv_ml=OFF
    -DBUILD_opencv_legacy=OFF
    -DBUILD_opencv_calib3d=OFF
    -DBUILD_opencv_features2d=OFF
    -DBUILD_opencv_java=OFF
    -DBUILD_opencv_objdetect=OFF
    -DBUILD_opencv_photo=OFF
    -DBUILD_opencv_nonfree=OFF
    -DBUILD_opencv_ocl=OFF
    -DBUILD_opencv_stitching=OFF
    -DBUILD_opencv_superres=OFF
    -DBUILD_opencv_ts=OFF
    -DBUILD_opencv_videostab=OFF
    -DBUILD_SHARED_LIBS:BOOL=OFF
    -DBUILD_TESTS:BOOL=OFF
    -DBUILD_PERF_TESTS:BOOL=OFF
    -DBUILD_opencv_contrib=OFF
    -DBUILD_WITH_CAROTENE=OFF
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DWITH_FFMPEG:BOOL=OFF
    -DWITH_IPP:BOOL=OFF
    -DWITH_ITT:BOOL=OFF
    -DWITH_TBB:BOOL=OFF
    -DWITH_OPENCL:BOOL=OFF
    -DWITH_CUDA:BOOL=OFF
    -DWITH_CUDNN:BOOL=OFF
    -DWITH_GSTREAMER:BOOL=OFF
    -DWITH_VTK:BOOL=OFF
    -DWITH_GTK:BOOL=OFF
    -DBUILD_PNG:BOOL=OFF
    -DBUILD_JPEG:BOOL=ON
    -DBUILD_ZLIB:BOOL=ON
    -DBUILD_WITH_STATIC_CRT:BOOL=ON
    -DBUILD_FAT_JAVA_LIB=OFF
    -DENABLE_PRECOMPILED_HEADERS=OFF
    -DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_CURRENT_BINARY_DIR}/thirdparty/opencv/
)

set(OpenCV_DIR ${CMAKE_CURRENT_BINARY_DIR}/thirdparty/opencv/)
link_directories(${OpenCV_DIR})

set(OPENCV_INCLUDE_DIR ${CMAKE_CURRENT_BINARY_DIR}/thirdparty/opencv/include/)
if(NOT WIN32)
  set(OPENCV_LIBRARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/thirdparty/opencv/lib/)
else()
  set(OPENCV_LIBRARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/thirdparty/opencv/x86/vc12/lib)
endif()

set(OPENCV_LIBRARIES opencv_imgproc opencv_core opencv_highgui opencv_video opencv_imgcodecs opencv_features2d)

include_directories(${OPENCV_INCLUDE_DIR})
link_directories(${OPENCV_LIBRARY_DIR})

add_custom_target(rerun ${CMAKE_COMMAND} ${CMAKE_SOURCE_DIR} DEPENDS opencv)
