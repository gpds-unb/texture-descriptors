function(add_library_test TEST_NAME TEST_BIN_NAME TEST_SOURCE)
        add_executable(${TEST_BIN_NAME} ${TEST_SOURCE})
        target_include_directories(${TEST_BIN_NAME} PRIVATE "${INCLUDE_DIRECTORIES};${open3d_INCLUDE};${OPENCV_INCLUDE_DIR};")
        target_link_libraries(${TEST_BIN_NAME} PRIVATE gtest_main)
        target_link_libraries(${TEST_BIN_NAME} PRIVATE ${OPENCV_LIBRARIES})
        target_link_libraries(${TEST_BIN_NAME} PRIVATE z)
        add_test(${TEST_NAME} ${EXECUTABLE_OUTPUT_PATH}/${TEST_BIN_NAME} 
                 "${CMAKE_HOME_DIRECTORY}/resources")
endfunction()


add_library_test(LBPTests lbp_tests LBPTests.cpp)