/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2019, Digital Signal Processing Group, GPDS, UnB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     OpenCVTests.cpp
 *  \brief    Tests for the external library Open3DTests
 *  \details  See more details about it at
 *            https://github.com/opencv/opencv
 *  \author   Pedro Garcia Freitas <pedro.gf@samsung.com>
 *  \date     2019-09-03
 */

#include <algorithm>
#include <string>
#include <vector>

#include "gtest/gtest.h"
#include "opencv2/core.hpp"
#include "opencv2/core/base.hpp"
#include "opencv2/imgproc.hpp"

using namespace std;
using namespace cv;

struct SimpleOpenCVNormalizeTest : public testing::Test {
 private:
  vector<double> result;

 protected:
  vector<double> expected;
  vector<double> input;
  cv::NormTypes norm;

  SimpleOpenCVNormalizeTest() : input({}), expected({}), norm(NORM_L1) {}

  void run() {
    normalize(input, result, 1.0, 0.0, norm);
    for (int i = 0; i <= expected.size(); i++)
      EXPECT_NEAR(expected[i], result[i], 0.1);
  }
};

TEST_F(SimpleOpenCVNormalizeTest, NORM_L1) {
  input = {2.0, 8.0, 10.0};
  expected = {0.1, 0.4, 0.5};
  norm = NORM_L1;
  run();
}

TEST_F(SimpleOpenCVNormalizeTest, NORM_L2) {
  input = {2.0, 8.0, 10.0};
  expected = {0.15, 0.62, 0.77};
  norm = NORM_L2;
  run();
}

TEST_F(SimpleOpenCVNormalizeTest, NORM_INF) {
  input = {2.0, 8.0, 10.0};
  expected = {0.2, 0.8, 1.0};
  norm = NORM_INF;
  run();
}

TEST_F(SimpleOpenCVNormalizeTest, NORM_MINMAX) {
  input = {2.0, 8.0, 10.0};
  expected = {0.0, 0.75, 1.0};
  norm = NORM_MINMAX;
  run();
}

TEST(Histogram, HistogramNormalization) {
  vector<float> input{2, 2, 3, 4, 1, 2};
  Mat histogram(1, 6, CV_32F);
  std::memcpy(histogram.data, input.data(), 1*6*sizeof(float));
  int histSize = 4;
  float range[] = {1, 5};
  const float* histRange = {range};
  bool uniform = true;
  bool accumulate = false;

  Mat hist;
  calcHist(&histogram, 1, 0, Mat(), hist, 1, &histSize, &histRange, true,
           false);
  normalize(hist, hist, 1, 0, NORM_L2, -1, Mat());

  vector<float> expected{0.28867513, 0.86602539, 0.28867513, 0.2886751};
  Mat expected_histogram(1, 4, CV_32F);
  std::memcpy(expected_histogram.data, expected.data(), 1*6*sizeof(float));

  for (int r = 0; r < expected_histogram.rows; r++)
    for (int c = 0; c < expected_histogram.cols; c++)
      EXPECT_NEAR(expected_histogram.at<float>(r, c), hist.at<float>(r, c),
                  0.001);
}

int main(int argc, char* argv[]) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
