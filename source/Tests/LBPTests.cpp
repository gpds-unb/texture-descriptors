/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2019, Digital Signal Processing Group (GPDS), UnB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     LBPTests.cpp
 *  \brief    Tests for basic LBP
 *  \author   Pedro Garcia Freitas <pedrogarcia@ieee.org>
 *  \date     2019-09-08
 */
#include <algorithm>
#include <cstdint>
#include <string>
#include <vector>
#include "Lib/DefaultLBPUnit.h"
#include "Lib/NonRotationInvariantUniformLBPUnit.h"
#include "Lib/RotationInvariantLBPUnit.h"
#include "Lib/RotationInvariantVarianceLBPUnit.h"
#include "Lib/UniformLBPUnit.h"
#include "gtest/gtest.h"


using namespace std;


TEST(DefaultLBPUnit, STLVectorInt) {
  vector<int> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  int center = 35;
  int computed = texdesc::DefaultLBPUnit<int>::getLabel(center, neighboors);
  EXPECT_EQ(13, computed);
}


TEST(DefaultLBPUnit, STLVectorUnsignedInt) {
  vector<unsigned int> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  unsigned int center = 35;
  unsigned int computed =
      texdesc::DefaultLBPUnit<unsigned int>::getLabel(center, neighboors);
  EXPECT_EQ(13, computed);
}


TEST(DefaultLBPUnit, STLVectorChar) {
  vector<char> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  char center = 35;
  char computed = texdesc::DefaultLBPUnit<char>::getLabel(center, neighboors);
  EXPECT_EQ(13, computed);
}


TEST(DefaultLBPUnit, STLVectorUint64_t) {
  vector<uint64_t> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  uint64_t center = 35;
  uint64_t computed =
      texdesc::DefaultLBPUnit<uint64_t>::getLabel(center, neighboors);
  EXPECT_EQ(13, computed);
}


TEST(DefaultLBPUnit, STLVectoruint8t) {
  vector<uint8_t> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  uint8_t center = 35;
  uint8_t computed =
      texdesc::DefaultLBPUnit<uint8_t>::getLabel(center, neighboors);
  EXPECT_EQ(13, computed);
}


TEST(RotationInvariantLBP, STLVectorInt) {
  vector<int> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  int center = 35;
  int computed =
      texdesc::RotationInvariantLBPUnit<int>::getLabel(center, neighboors);
  EXPECT_EQ(13, computed);
}


TEST(RotationInvariantVarianceLBPUnit, STLVectorInt) {
  vector<int> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  int center = 35;
  int computed = texdesc::RotationInvariantVarianceLBPUnit<int>::getLabel(
      center, neighboors);
  EXPECT_EQ(1120, computed);
}


TEST(UniformLBPUnit, STLVectorInt) {
  vector<int> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  int center = 35;
  int computed = texdesc::UniformLBPUnit<int>::getLabel(center, neighboors);
  EXPECT_EQ(9, computed);
}


TEST(NonRotationInvariantUniformLBPUnit, STLVectorInt) {
  vector<int> neighboors{71, 32, 91, 103, 21, 14, 34, 13};
  int center = 35;
  int computed = texdesc::NonRotationInvariantUniformLBPUnit<int>::getLabel(
      center, neighboors);
  EXPECT_EQ(58, computed);
}


int main(int argc, char *argv[]) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
