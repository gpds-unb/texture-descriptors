/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2019, Digital Signal Processing Group, GPDS, UnB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     NonRotarionInvariantNonRotationInvariantUniformLBPUnit.h
 *  \brief    
 *  \author   Pedro Garcia Freitas <pedrogarcia@ieee.org>
 *  \date     2019-09-15
 */

#ifndef TEXTUREDESCRIPTORS_NONROTARIONINVARIANTUNIFORMLBPUNIT_H
#define TEXTUREDESCRIPTORS_NONROTARIONINVARIANTUNIFORMLBPUNIT_H

#include <cmath>
#include <cstdint>
#include <iostream>
#include <type_traits>
#include <vector>
#include "AbstractLBPUnit.h"
#include "cppitertools/range.hpp"

using iter::range;
using namespace std;


namespace texdesc {

template<typename T,
    class = typename std::enable_if<std::is_integral<T>::value>::type>
class NonRotationInvariantUniformLBPUnit : public AbstractLBPUnit<T> {
 public:
  static T getLabel(T center, vector<T> neighbors) {
    NonRotationInvariantUniformLBPUnit<T> p(center, neighbors);
    return p.getCode();
  }

 protected:
  NonRotationInvariantUniformLBPUnit(T center, vector<T> neighbors)
      : AbstractLBPUnit<T>(center, neighbors){};

  T getCode() override {
    using Parent = AbstractLBPUnit<T>;
    unsigned int P = Parent::P;
    T lbp = 0;
    unsigned int changes = 0;
    for (auto i : range(P - 1))
      changes +=
          (Parent::signed_texture[i] - Parent::signed_texture[i + 1]) != 0;
    if (changes > 2) {
      lbp = P * (P - 1) + 2;
    } else {
      unsigned int n_ones = 0;
      int first_one = -1;
      int first_zero = -1;
      for (auto i : range(P)) {
        if (Parent::signed_texture[i] != 0) {
          n_ones += 1;
          if (first_one == -1)
            first_one = i;
        } else {
          if (first_zero == -1)
            first_zero = i;
        }
      }
      if (n_ones == 0)
        lbp = 0;
      else if (n_ones == P)
        lbp = P * (P - 1) + 1;
      else {
        int rot_index = (first_one == 0) ? n_ones - first_zero : P - first_one;
        lbp = 1 + (n_ones - 1) * P + rot_index;
      }
    }
    return lbp;
  }
};

}  // namespace texdesc

#endif  //TEXTUREDESCRIPTORS_NONROTARIONINVARIANTUNIFORMLBPUNIT_H
