/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2019, Digital Signal Processing Group, GPDS, UnB
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ITU/ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/** \file     AbstractLBPUnit.h
 *  \brief    
 *  \author   Pedro Garcia Freitas <pedrogarcia@ieee.org>
 *  \date     2019-09-14
 */

#ifndef TEXTUREDESCRIPTORS_ABSTRACTLBPUNIT_H
#define TEXTUREDESCRIPTORS_ABSTRACTLBPUNIT_H

#include <type_traits>
#include <vector>
#include "cppitertools/imap.hpp"
#include "cppitertools/range.hpp"

using iter::imap;
using iter::range;
using namespace std;

namespace texdesc {

template<typename T,
    class = typename std::enable_if<std::is_integral<T>::value>::type>
class AbstractLBPUnit {
 public:
  template<typename U,
      class = typename std::enable_if<std::is_integral<U>::value>::type>
  static U getLabel(U center, std::vector<U> neighbors) {
  }

 protected:
  vector<T> signed_texture;
  vector<T> texture;
  vector<T> weights;
  size_t P;

  AbstractLBPUnit(T center, vector<T> neighbors) {
    P = neighbors.size();
    const auto _weights = range(P) | imap([](auto x) { return exp2(x); });
    const auto _signed_texture = neighbors | imap([center](auto x) {
      return static_cast<T>(x > center);
    });
    texture = neighbors;
    std::copy(
        std::begin(_weights), std::end(_weights), std::back_inserter(weights));
    std::copy(std::begin(_signed_texture), std::end(_signed_texture),
        std::back_inserter(signed_texture));
  };

  virtual T getCode() = 0;
};

}  // namespace texdesc

#endif  //TEXTUREDESCRIPTORS_ABSTRACTLBPUNIT_H
